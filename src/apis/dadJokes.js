import axios from 'axios'

const BASE_URL = 'https://icanhazdadjoke.com'

export default axios.create({
  baseURL: BASE_URL,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
})
