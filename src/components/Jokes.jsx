import React from 'react'
import { useAxios } from '../hooks/useAxios'
import axios from '../apis/dadJokes'

export const Jokes = () => {
	const [response, error, loading, refetch] = useAxios({
		axiosInstance: axios,
		method: 'GET',
		url: '/',
		requestConfig:{
			headers:{
				'Content-Language': 'en-US',
			},
		}
	})
	return (
		<article>
			<h2>Random Dad Joke</h2>
			{
				loading && <p>Loading...</p>
			}		

			{
				!loading && error && <p className='errmsg'>Error: {error}</p>
			}

			{
				!loading && !error && response && <p>{response?.joke}</p>
			}

			{
				!loading && !error && !response && <p>No dad joke to display</p>
			}

			<button onClick={() => refetch()}>Get A Joke</button>
		</article>
	)
}
