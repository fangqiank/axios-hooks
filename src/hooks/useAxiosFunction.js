import { useState, useEffect } from 'react'

export const useAxiosFunction = () => {
  const [response, setResponse] = useState(null)
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)
  const [controller, setController] = useState()

  const axiosFetch = async (config) => {
    const { axiosInstance, method, url, requestConfig = {} } = config

    try {
      setLoading(true)
      const ctrl = new AbortController()
      setController(ctrl)
      // const res = await axiosInstance({
      //   url,
      //   method: method.toLowerCase(),
      //   ...requestConfig,
      //   signal: ctrl.signal,
      // })
      const res = await axiosInstance[method.toLowerCase()](url, {
        ...requestConfig,
        signal: ctrl.signal,
      })
      setResponse(res.data)
      console.log(typeof axiosInstance)
    } catch (err) {
      console.log(err)
      setError(err.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    // console.log({ controller })

    return () => controller && controller.abort()
  }, [controller])

  return [response, error, loading, axiosFetch]
}
