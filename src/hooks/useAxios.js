import { useState, useEffect } from 'react'

export const useAxios = (config) => {
  const { axiosInstance, method, url, requestConfig = {} } = config

  const [response, setResponse] = useState(null)
  const [error, setError] = useState('')
  const [loading, setloading] = useState(true)
  const [reload, setReload] = useState(0)

  const refetch = () =>
    setReload((prev) => {
      console.log({ prev })
      return prev + 1
    })

  useEffect(() => {
    const controller = new AbortController()

    controller ? console.log(controller) : null

    const fetchData = async () => {
      try {
        const res = await axiosInstance({
          url,
          method: method.toLowerCase(),
          ...requestConfig,
          signal: controller.signal,
        })
        console.log(res.data)
        setResponse(res.data)
      } catch (err) {
        console.log(err)
        setError(err.message)
      } finally {
        setloading(false)
      }
    }

    fetchData()

    return () => controller.abort()
  }, [reload])

  return [response, error, loading, refetch]
}
